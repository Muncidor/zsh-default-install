#!/bin/bash

set -e

add_keys=true
zshrc_file=~/.zshrc

# Mise à jour et mise à niveau des paquets
apt update && apt upgrade -y

# Installation des paquets nécessaires
apt install -y sudo vim zsh curl

# Fonction pour ajouter une ligne à ~/.zshrc si elle n'existe pas
add_to_zshrc() {
  local line=$1
  grep -qF -- "$line" "$zshrc_file" || echo "$line" >> "$zshrc_file"
}

# Ajouter des commandes et des alias à ~/.zshrc
add_to_zshrc 'export PATH="$PATH:~/.local/bin"'
add_to_zshrc "alias maj='sudo apt update -y && sudo apt upgrade -y && sudo apt full-upgrade -y && sudo apt dist-upgrade -y'"
add_to_zshrc "alias purge='sudo apt autoremove -y && sudo apt autoclean -y && sudo apt remove -y && sudo apt clean -y && sudo apt-get autoremove -y'"

# Traitement des options
while getopts ":ah" opt; do
  case $opt in
    a)
      add_keys=false
      ;;
    h)
      echo "Usage: $0 [-a] [-h]"
      echo "Options:"
      echo "  -a     Ne pas ajouter de clés SSH autorisées."
      echo "  -h     Afficher cette aide."
      exit 0
      ;;
    \?)
      echo "Option invalide: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

if [ "$add_keys" = true ]; then
    # Ajouter des clés SSH autorisées avec gestion d'erreur
    mkdir -p ~/.ssh

    authorized_keys_file=~/.ssh/authorized_keys
    ssh_keys=("ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMYK5cXPp+jqS4LpN4TcEYuRQ7RKbAGt3JtHtP9lx/7c from_PI"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGvpI2g7NRsQj4X7HrZVsjYLFK0nSL0sWj1kXmzn7P0H debian56"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIBOR4GWTSajp1zHSGmuk+krd6r4uqc2S6ByA/uGwo5J FromWork"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFB66mUF9/gi03r7VwwmPotHFK6n1UtHWvMtNEKxqMIl fromWork"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJyj8Ti0re3dbnu9mPi6BDg/eeQVPH58LQYYCdzw4Yz1"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICKoNS5iht/sp7Se1a5vq4LLuCdMsXUYeUudpWTkvKWa Munly56"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPzlAbry1fIRSyv8sPJ2q3YRvdL7RnvjLyQYYkDQII5y munly51")

    for key in "${ssh_keys[@]}"; do
        grep -qF -- "$key" "$authorized_keys_file" || echo "$key" >> "$authorized_keys_file"
    done
fi

# Installation de Oh My Zsh sans intervention
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

# Changer le shell par défaut en zsh
chsh -s $(which zsh)

echo "Installation terminée avec succès !"