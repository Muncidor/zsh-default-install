#!/bin/bash

# Fonction d'affichage de l'aide
display_help() {
    echo "Usage: $0"
    echo "This script will secure your SSH configuration!"
    echo
    echo "Options:"
    echo "  -h, --help        Display this help message."
}

# Gestion des options en ligne de commande
while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -h|--help)
            display_help
            exit 0
            ;;
        *)
            echo "Invalid option: $1"
            display_help
            exit 1
            ;;
    esac
done

# Étape 1: Modifier le fichier /etc/ssh/sshd_config

# Port 34231
sudo sed -i 's/#Port 22/Port 34231/' /etc/ssh/sshd_config

# Protocol 2
if ! grep -q "^Protocol 2" /etc/ssh/sshd_config; then
    echo "Protocol 2" | sudo tee -a /etc/ssh/sshd_config > /dev/null
else
    sudo sed -i 's/#Protocol 2/Protocol 2/' /etc/ssh/sshd_config
fi

# PermitRootLogin no
if grep -q "^PermitRootLogin" /etc/ssh/sshd_config; then
    sudo sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
else
    echo "PermitRootLogin no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# LoginGraceTime 20s
sudo sed -i 's/#LoginGraceTime 2m/LoginGraceTime 20s/' /etc/ssh/sshd_config

# # MaxAuthTries 1
# sudo sed -i 's/#MaxAuthTries 6/MaxAuthTries 1/' /etc/ssh/sshd_config

# PubkeyAuthentication yes
if grep -q "^#PubkeyAuthentication" /etc/ssh/sshd_config; then
    sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config
elif ! grep -q "^PubkeyAuthentication" /etc/ssh/sshd_config; then
    echo "PubkeyAuthentication yes" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# UsePAM no
if grep -q "^UsePAM" /etc/ssh/sshd_config; then
    sudo sed -i 's/UsePAM yes/UsePAM no/' /etc/ssh/sshd_config
else
    echo "UsePAM no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# KerberosAuthentication no
if grep -q "^KerberosAuthentication" /etc/ssh/sshd_config; then
    sudo sed -i 's/KerberosAuthentication yes/KerberosAuthentication no/' /etc/ssh/sshd_config
else
    echo "KerberosAuthentication no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# GSSAPIAuthentication no
if grep -q "^GSSAPIAuthentication" /etc/ssh/sshd_config; then
    sudo sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/' /etc/ssh/sshd_config
else
    echo "GSSAPIAuthentication no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# PasswordAuthentication no
if grep -q "^PasswordAuthentication" /etc/ssh/sshd_config; then
    sudo sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
else
    echo "PasswordAuthentication no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# PermitEmptyPasswords no
if grep -q "^PermitEmptyPasswords" /etc/ssh/sshd_config; then
    sudo sed -i 's/PermitEmptyPasswords yes/PermitEmptyPasswords no/' /etc/ssh/sshd_config
else
    echo "PermitEmptyPasswords no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# # X11Forwarding no
# if grep -q "^X11Forwarding" /etc/ssh/sshd_config; then
#     sudo sed -i 's/X11Forwarding yes/X11Forwarding no/' /etc/ssh/sshd_config
# else
#     echo "X11Forwarding no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
# fi

# # PermitTunnel no
# if grep -q "^PermitTunnel" /etc/ssh/sshd_config; then
#     sudo sed -i 's/PermitTunnel yes/PermitTunnel no/' /etc/ssh/sshd_config
# else
#     echo "PermitTunnel no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
# fi

# DebianBanner no
if grep -q "^DebianBanner" /etc/ssh/sshd_config; then
    sudo sed -i 's/DebianBanner yes/DebianBanner no/' /etc/ssh/sshd_config
else
    echo "DebianBanner no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
fi

# Supprimer les algorithmes recommandés
sudo sed -i '/^KexAlgorithms/ d' /etc/ssh/sshd_config
sudo sed -i '/^HostKeyAlgorithms/ d' /etc/ssh/sshd_config
sudo sed -i '/^MACs/ d' /etc/ssh/sshd_config

# Ajouter les algorithmes recommandés
echo "KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256" | sudo tee -a /etc/ssh/sshd_config > /dev/null
echo "HostKeyAlgorithms ssh-ed25519" | sudo tee -a /etc/ssh/sshd_config > /dev/null
echo "MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# # Désactiver la compression
# if grep -q "^Compression" /etc/ssh/sshd_config; then
#     sudo sed -i 's/Compression yes/Compression no/' /etc/ssh/sshd_config
# else
#     echo "Compression no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
# fi

# Limiter les utilisateurs autorisés (optionnel)
# echo "AllowUsers youruser" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# Étape 2: Modifier les droits sur les fichiers de clés publiques

# Ajouter des clés SSH autorisées pour l'utilisateur actuel
mkdir -p ~/.ssh
touch ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys

# Redémarrer le service SSH pour appliquer les modifications
sudo systemctl restart sshd

# Étape 3: Configurer le pare-feu UFW pour autoriser le port 34231

sudo ufw allow 34231 || { echo "Failed to configure UFW"; exit 1; }
sudo ufw reload || { echo "Failed to reload UFW"; exit 1; }
sudo ufw status

# Afficher les configurations mises à jour
echo "SSH configuration updated successfully and port 34231 allowed in UFW!"
