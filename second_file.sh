#!/bin/bash

# Chemin d'installation des plugins Zsh
plugins_dir=~/.oh-my-zsh/custom/plugins
zshrc_file=~/.zshrc

# Fonction pour cloner un plugin avec gestion d'erreur
clone_plugin() {
  local repo_url=$1
  local target_dir=$2
  if [ ! -d "$target_dir" ]; then
    git clone "$repo_url" "$target_dir"
    if [ $? -ne 0 ]; then
      echo "Erreur: impossible de cloner $repo_url"
      exit 1
    fi
  fi
}

# Mise à jour initiale du système
sudo apt update -y && sudo apt upgrade -y

# # Configurer le pare-feu
# sudo ufw allow OpenSSH
# sudo ufw enable

# Renforcer la sécurité SSH
sudo sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
sudo systemctl restart ssh

# Cloner les plugins
clone_plugin "https://github.com/zsh-users/zsh-autosuggestions.git" "$plugins_dir/zsh-autosuggestions"
clone_plugin "https://github.com/zsh-users/zsh-syntax-highlighting.git" "$plugins_dir/zsh-syntax-highlighting"
clone_plugin "https://github.com/ptavares/zsh-exa.git" "$plugins_dir/zsh-exa"

# Modifier le thème Zsh avec gestion d'erreur
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="lukerandall"/' "$zshrc_file"
if [ $? -ne 0 ]; then
  echo "Erreur: impossible de modifier le thème Zsh dans $zshrc_file"
  exit 1
fi

# Ajouter les plugins au fichier ~/.zshrc avec gestion d'erreur
sed -i 's/plugins=(git)/plugins=(git sudo fzf zsh-autosuggestions zsh-syntax-highlighting zsh-exa)/' "$zshrc_file"
if [ $? -ne 0 ]; then
  echo "Erreur: impossible d'ajouter les plugins dans $zshrc_file"
  exit 1
fi

# Fonction pour ajouter une ligne à ~/.zshrc si elle n'existe pas
add_to_zshrc() {
  local line=$1
  grep -qF -- "$line" "$zshrc_file" || echo "$line" >> "$zshrc_file"
}

# Ajouter des commandes et des alias à ~/.zshrc
add_to_zshrc 'export PATH="$PATH:~/.local/bin"'
add_to_zshrc "alias maj='sudo apt update -y && sudo apt upgrade -y && sudo apt full-upgrade -y && sudo apt dist-upgrade -y && omz update'"
add_to_zshrc "alias purge='sudo apt autoremove -y && sudo apt autoclean -y && sudo apt remove -y && sudo apt clean -y && sudo apt-get autoremove -y'"
add_to_zshrc "alias bat='batcat -p --paging=never'"
add_to_zshrc "alias t='tmux'"
add_to_zshrc "alias ta='tmux attach'"

# Ajouter des clés SSH autorisées avec gestion d'erreur
mkdir -p ~/.ssh
if [ $? -ne 0 ]; then
  echo "Erreur: impossible de créer le répertoire ~/.ssh"
  exit 1
fi

authorized_keys_file=~/.ssh/authorized_keys
ssh_keys=("ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMYK5cXPp+jqS4LpN4TcEYuRQ7RKbAGt3JtHtP9lx/7c from_PI
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICKoNS5iht/sp7Se1a5vq4LLuCdMsXUYeUudpWTkvKWa Munly56
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID6tqoU60n8KapcPr4yXb/Y5Vltuf25IugjDWzD4rErH Yoga
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFB66mUF9/gi03r7VwwmPotHFK6n1UtHWvMtNEKxqMIl fromWork
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFppUS49X1y64w09BVGn3NwiZzBSUhPw5x5NIrGpmujM proxmox
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFTREhJPvWhb+g0brkWvzAQ+oMbqJaPvQc8zKO5Vcu7v surface
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGh87KmQ30jaY3bztPzcgjPjFB75sZJnVx4VqIwgDX6I debMunLy56
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGvpI2g7NRsQj4X7HrZVsjYLFK0nSL0sWj1kXmzn7P0H debian56
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHCBvhUQDKjilAEQyy9a2QHWd3vxq3qGF3p/4Cld0bUU ubuntu51
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHUUAUQKPXIVLldn3okgLdLDxg3r2PyEvadipGUNXQ/2 pi2
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJyj8Ti0re3dbnu9mPi6BDg/eeQVPH58LQYYCdzw4Yz1
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM4DzWW25T9mGycFw4/z5fS6Lh12q8AdL+xVd+MolwkJ Server
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM8QMx6+9j+vtafAhUON5tCHXzz67IVN0lyoJQ6j+xCa framboise
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN3gfxDJ/jAg7SDDkx/bDDDxCIXum67iyU3r88nUM+zI Kali-Precision
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN503gajaPS0+mtKHQEvluTgG5ltkAe32sRq2ePGePUx
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPzlAbry1fIRSyv8sPJ2q3YRvdL7RnvjLyQYYkDQII5y munly51")

for key in "${ssh_keys[@]}"; do
  grep -qF -- "$key" "$authorized_keys_file" || echo "$key" >> "$authorized_keys_file"
  if [ $? -ne 0 ]; then
    echo "Erreur: impossible d'ajouter la clé SSH $key"
    exit 1
  fi
done

# Supprimer les doublons du PATH avec gestion d'erreur
remove_duplicates_from_path='export PATH=$(echo -n $PATH | awk -v RS=":" '\''{ if (!x[$0]++) { printf s $0 ; s=":" } }'\'')'
add_to_zshrc '# Remove duplicate entries from PATH'
add_to_zshrc "$remove_duplicates_from_path"

# Ajouter une tâche cron pour les mises à jour et le nettoyage réguliers
add_cron_jobs() {
  (sudo crontab -l 2>/dev/null; echo "0 2 * * * sudo apt update -y && sudo apt upgrade -y && sudo apt full-upgrade -y && sudo apt dist-upgrade -y && omz update") | sudo crontab -
  if [ $? -ne 0 ]; then
    echo "Erreur: impossible d'ajouter la tâche 'maj' à la crontab de root"
    exit 1
  fi

  (sudo crontab -l 2>/dev/null; echo "0 3 * * * sudo apt autoremove -y && sudo apt autoclean -y && sudo apt remove -y && sudo apt clean -y && sudo apt-get autoremove -y") | sudo crontab -
  if [ $? -ne 0 ]; then
    echo "Erreur: impossible d'ajouter la tâche 'purge' à la crontab de root"
    exit 1
  fi
}

# Ajouter les tâches cron par défaut
add_cron_jobs

# Recharger le fichier ~/.zshrc avec gestion d'erreur
source "$zshrc_file"
if [ $? -ne 0 ]; then
  echo "Erreur: impossible de recharger $zshrc_file"
  exit 1
fi

# # Fonction pour exécuter le script secure_ssh.sh
# execute_secure_ssh() {
#     local script_path="./secure_ssh.sh"

#     if [[ -f "$script_path" ]]; then
#         echo "Le script secure_ssh.sh a été trouvé. Exécution du script..."
#         bash "$script_path"
#         if [[ $? -eq 0 ]]; then
#             echo "Le script secure_ssh.sh a été exécuté avec succès."
#         else
#             echo "Une erreur s'est produite lors de l'exécution du script secure_ssh.sh."
#         fi
#     else
#         echo "Le script secure_ssh.sh n'a pas été trouvé dans le dossier courant."
#     fi
# }

# script secure_ssh.sh execution
# execute_secure_ssh

echo "Configuration initiale terminée avec succès."
