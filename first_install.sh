#!/bin/bash

# Fonction pour afficher l'aide
show_help() {
    echo "Usage: $0 [options]"
    echo "Options:"
    echo "  -h, --help      Affiche ce message d'aide et quitte."
    echo "  -l, --light     Installe seulement zsh, tmux, sudo, git, htop, wget, curl, bat et unzip."
}

# Liste des paquets complets
full_packages="ufw fzf neofetch tmux build-essential pixz keychain pigz zsh xz-utils htop vim curl git wget bat unzip"

# Liste des paquets légers
light_packages="zsh tmux sudo git htop wget curl bat unzip vim btop"

# Parse des options
if [[ $# -gt 0 ]]; then
    case $1 in
        -h|--help)
            show_help
            exit 0
            ;;
        -l|--light)
            packages=$light_packages
            ;;
        *)
            echo "Option inconnue : $1"
            show_help
            exit 1
            ;;
    esac
else
    packages=$full_packages
fi

# Installation des paquets avec sudo
sudo apt update
sudo apt install $packages -y

# Installation de Oh My Zsh si zsh est installé
if [[ $packages == *"zsh"* ]]; then
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi
